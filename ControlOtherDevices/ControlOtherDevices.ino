#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

//Setup Wifi
const char* ssid = "BKHCM_OISP";
const char* password = "bachkhoaquocte";


//Setup DHT sensor
#include "DHT.h"
#define DHTPIN 4
#define DHTTYPE DHT11
unsigned long timeTeHu = 0;
String Temp, Humi, Final;
char packet[15];
char incomingPacket[255];
DHT dht(DHTPIN, DHTTYPE);
int h, t;

//Setup UDP
WiFiUDP Udp;
IPAddress serverIP(172, 28, 19, 248);
unsigned int localUdpPort = 8888;
unsigned int serverPort = 1111;

int tv = 12;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println();
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println(" connected");
  Serial.printf("Now listening at IP %s, UDP port %d\n", WiFi.localIP().toString().c_str(), localUdpPort);
  dht.begin();
  pinMode (tv, OUTPUT);
  Udp.begin(localUdpPort);
}

void loop() {
  if ((unsigned long) (millis() - timeTeHu) > 1000)
  {
    send_temp_humi();
    timeTeHu = millis();
  }
  controlIR();
}

void controlIR()
{
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {
    //receive incoming UDP packets
    Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
    int len = Udp.read(incomingPacket, 255);
    if (len > 0)
    {
      incomingPacket[len] = 0;
    }
    Serial.printf("UDP packet contents: %s\n", incomingPacket);
    
    Final = String(incomingPacket);
    if (Final == "tv")
    {
      digitalWrite(tv, HIGH);
      delay(200);
      digitalWrite(tv, LOW);
    }
  }
}

void send_temp_humi()
{
  // put your main code here, to run repeatedly:
  h = dht.readHumidity();
  t = dht.readTemperature();
  if (isnan(h) || isnan(t)) {
    return;
  }
  if (h != 2147483647 & t != 2147483647)
  {
    Temp = String(t);
    Humi = String(h);

    Final = "Temp&Humi|" + Temp +  ';' + Humi;
    Final.toCharArray(packet, 20);
    Udp.beginPacket(serverIP, serverPort);

    Udp.write(packet);
    Udp.endPacket();
    Serial.println(Temp + "|" + Humi);
  }
}
