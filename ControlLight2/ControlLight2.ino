#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

//Setup initial time for upload state of current
int check_current = 0;

//Detect 0.29A signal
int check_trigger = 0;

//Setup wifi
const char* ssid = "BKHCM_OISP";
const char* password = "bachkhoaquocte";

//Setup current sensor
const int current = A0;
int sensivity = 185;//66 for 30A; 185 for 5A
double Voltage = 0;
double Ampere = 0;
double value = 0.25;

//Setup port to control light
int light2 = 16;

//Send and receive data by UDP
WiFiUDP Udp;
IPAddress serverIP(172, 28, 19, 248);
unsigned int serverPort = 1111; 
unsigned int localUdpPort = 8888;
String light;
char packet[20]; 
char incomingPacket[255]; 

void setup() {
  Serial.begin(9600);
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println(" connected");
  Serial.printf("Now listening at IP %s, UDP port %d\n", WiFi.localIP().toString().c_str(), localUdpPort);
  
  Udp.begin(localUdpPort);
  
  pinMode (light2, OUTPUT);
  
  delay(500);
  current_state();
}

void loop() {
  control_light();
  if ((unsigned long) (millis() - check_current) > 15000)
  {
    control_light();
    current_state();
    check_current = millis();
  }
}

int control_light()
{
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {
    calculate(500);
    Serial.println();
    Serial.print("Ampere in control_light() function: ");
    Serial.println(Ampere);
    
    //receive incoming UDP packets
    Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
    int len = Udp.read(incomingPacket, 255);
    if (len > 0)
    {
      incomingPacket[len] = 0;
    }
    Serial.printf("UDP packet contents: %s\n", incomingPacket);
    
    light = String(incomingPacket);
    if (light == "light2|1")
    {
      if (Ampere < value & digitalRead(light2) == LOW)
      {
        digitalWrite(light2, HIGH);
        return 1;
      }
      else if (Ampere < value & digitalRead(light2) == HIGH)
      {
        digitalWrite(light2, LOW);
        return 1;
      }
    }
    else if (light ==  "light2|0")
    {
      
      if (Ampere >= value & digitalRead(light2) == HIGH)
      {
        digitalWrite(light2, LOW);
        return 1;
        
      }
      else if (Ampere >= value & digitalRead(light2) == LOW)
      {
        digitalWrite(light2, HIGH);
        return 1;
      }
    }
  }
  return 0;
}

void calculate (int sample_time)
{
  double result;
  int readValue;
  int maxValue = 0;
  int minValue = 1024;
  uint32_t start_time = millis();

  while ((millis()- start_time) < sample_time)
  {
    readValue = analogRead(current);
    if (readValue > maxValue)
    {
      maxValue = readValue;
    }
    if (readValue < minValue)
    {
      minValue = readValue;
    }
  }
  
  result = ((maxValue - minValue) *5.0)/1024.0;
  Voltage = (result/2.0)*0.707;
  Ampere = (Voltage*1000)/sensivity;
}

void current_state()
{
  calculate(500);
  Serial.println();
  Serial.print("Ampere in current_state() function: ");
  Serial.println(Ampere);
  
  if (Ampere >= value)
  {
    send_data("current2|1");
    Serial.println("send @on state to server");
  }
  else if (Ampere < value)
  {
    send_data("current2|0");
    Serial.println("send @off state to server");
  }
}

void send_data(String message)
{
  Udp.beginPacket(serverIP, serverPort);
  message.toCharArray(packet,20);
  Udp.write(packet);
  Udp.endPacket();
}
